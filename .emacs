;; org directory and os specific config
(cond
  ((eq system-type 'windows-nt)
   (setq org-directory (concat (getenv "USERPROFILE") "\\OneDrive - Alcon\\notes\\"))
  )
  ((eq system-type 'darwin)
   (setq org-directory (concat "/Users/" (getenv "USER") "/Documents/notes/"))
   (setq mac-option-modifier 'super)
   (setq mac-command-modifier 'meta)
  )
  ((eq system-type 'gnu/linux)
   (if (string-match "termux" (getenv "HOME"))
       (setq org-directory "/data/data/com.termux/files/home/storage/shared/Notes/")
       (setq org-directory (concat "/home/" (getenv "USER") "/Documents/notes/")))
  )
 )

;; org babel
(org-babel-do-load-languages
 'org-babel-load-languages
 '((python . t)
   (shell . t)))
(setq org-confirm-babel-evaluate nil)

;; org other
(setq org-image-actual-width 500)
(add-hook 'org-mode-hook 'org-indent-mode)
(setq org-hide-leading-stars t)
(setq org-M-RET-may-split-line nil)
(setq org-return-follows-link t)
(add-to-list 'org-link-frame-setup '(file . find-file))

;; encoding
(prefer-coding-system 'utf-8)
(setq coding-system-for-read 'utf-8)
(setq coding-system-for-write 'utf-8)

;; visual config
(if (string-match "termux" (getenv "HOME"))
  nil
  (progn
    (tool-bar-mode -1)
    (scroll-bar-mode -1)
  )
)
(column-number-mode t)
(blink-cursor-mode 0)
;(menu-bar-mode -1)
(setq ring-bell-function 'ignore)
(setq inhibit-startup-screen t)
(setq inhibit-startup-echo-area-message t)
(global-auto-revert-mode t)
(global-visual-line-mode t)

;; other config
(setq backup-directory-alist '(("" . "~/.emacs.d/backup")))
(delete-selection-mode t)
(savehist-mode 1)

;; package manager
(require 'package)
(add-to-list 'package-archives '("gnu" . "https://elpa.gnu.org/packages/"))
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))
(add-to-list 'package-archives '("melpa-stable" . "https://stable.melpa.org/packages/") t)
(package-initialize)

(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))
(require 'use-package-ensure)
(eval-and-compile
   (setq use-package-always-ensure t
        use-package-expand-minimally t))

;;; PACKAGES
(use-package org-roam
  :defer t
  :custom
    (org-roam-directory (file-truename org-directory))
    (org-roam-dailies-directory (file-truename org-directory))
  :bind (
      ;; Notes
      ("C-c f" . 'org-roam-node-find)
      ("C-c i" . 'org-roam-node-insert)
      ("C-c c" . 'org-roam-capture)
      ("C-c a t" . 'org-roam-tag-add)
      ("C-c a a" . 'org-roam-alias-add)
      ;; Dailies
      ("C-c t" . 'org-roam-dailies-goto-today)
      ("C-c y" . 'org-roam-dailies-goto-yesterday)
      ("C-c w" . 'org-roam-dailies-goto-tomorrow)
      ("C-c n" . 'org-roam-dailies-goto-next-note)
      ("C-c p" . 'org-roam-dailies-goto-previous-note)
      ("C-c d" . 'org-roam-dailies-goto-date)
    )
  :config (org-roam-db-autosync-mode)
  )
(when (display-graphic-p)
  (progn
    (use-package org-roam-ui
      :after org-roam)
    (use-package char-fold
      :custom
      (char-fold-symmetric t)
      (search-default-mode #'char-fold-to-regexp))
    (use-package reverse-im
      :demand t
      :after char-fold
      :custom
        (reverse-im-char-fold t)
        (reverse-im-read-char-advice-function #'reverse-im-read-char-include)
        (reverse-im-input-methods '("russian-computer"))
      :config
        (reverse-im-mode t))
  )
)
(use-package magit
  :bind (("C-x g" . magit-status)
         ("C-c g" . magit-status)))
(use-package nov
  :pin melpa-stable
  :mode ("\\.epub\\'" . nov-mode))
(use-package fb2-reader
  :mode ("\\.fb2\\(\\.zip\\)?\\'" . fb2-reader-mode)
  :commands (fb2-reader-continue)
  :custom
  ;; This mode renders book with fixed width, adjust to your preferences.
  (fb2-reader-page-width 50)
  (fb2-reader-image-max-width 400)
  (fb2-reader-image-max-height 400))
(use-package vertico
  :config
  (setq read-file-name-completion-ignore-case t
	read-buffer-completion-ignore-case t
	completion-ignore-case t)
  (setq completion-styles '(substring orderless basic)))
(use-package vertico-directory
  :after vertico
  :ensure nil
  ;; More convenient directory navigation commands
  :bind (:map vertico-map
	      ("RET" . vertico-directory-enter)
	      ("DEL" . vertico-directory-delete-char)
	      ("M-DEL" . vertico-directory-delete-word))
  ;; Tidy shadowed file names
    :hook (rfn-eshadow-update-overlay . vertico-directory-tidy))
(use-package company
  :pin melpa-stable
  :config
    (company-tng-configure-default)
    (setq company-selection-wrap-around t)
    (setq company-idle-delay 0)
    (setq company-minimum-prefix-length 2)
    (setq company-backends
    	'((company-files          ; files & directory
    	   company-keywords       ; keywords
    	   company-capf
    	   ;company-yasnippet
    	   )
    	  (company-abbrev company-dabbrev)
    	          ))
    (add-hook 'after-init-hook 'global-company-mode))
(use-package markdown-mode :defer t)
(use-package json-mode :defer t)
(use-package yaml-mode :defer t)
(use-package qrencode :defer t)
(use-package evil
  :config
    ;(require 'evil)
    (evil-mode 1)
    ;; Use Emacs Mode instead of Insert Mode
    (setq evil-insert-state-map (make-sparse-keymap))
    (define-key evil-insert-state-map (kbd "<escape>") 'evil-normal-state)
    ;; Use emacs mode as default
    ;(setq evil-default-state 'insert)
  )
(use-package doom-themes
  :config
  (load-theme 'doom-one t)
  (doom-themes-org-config)
  )

;;; START SERVER
(load "server")
(unless (server-running-p) (server-start))

;;; KEY BINDING
;; Recent files
(recentf-mode)
(global-set-key (kbd "C-x C-r") 'recentf-open-files)
;; windows and buffers
(if (string-match "termux" (getenv "HOME"))
    nil
    (progn
        (global-set-key (kbd "M-[") 'previous-buffer)
        (global-set-key (kbd "M-]") 'next-buffer)
    )
)

;----------------------------------------------------------------------
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   '(doom-themes evil qrencode yaml-mode json-mode markdown-mode company vertico fb2-reader nov magit org-roam use-package)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
